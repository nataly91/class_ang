export class Product {
    pid: number;
    pname: string;
    cost: string;
    categoryId: number;
}
