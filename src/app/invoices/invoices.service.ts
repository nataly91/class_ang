import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {
 
  invoicesObservable;
  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
    return this.invoicesObservable;
 
  }

  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }

  
    
  constructor(private af:AngularFire) { }

}
