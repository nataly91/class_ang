import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import {ProductsService} from './products/products.service';
import {InvoicesService} from './invoices/invoices.service';
import {InvoiceComponent} from './invoice/invoice.component';


export const firebaseConfig = {
    apiKey: "AIzaSyBPB3f1iO-0oem2SLbE-yPrpps5FDSlenM",
    authDomain: "angular-9564b.firebaseapp.com",
    databaseURL: "https://angular-9564b.firebaseio.com",
    storageBucket: "angular-9564b.appspot.com",
    messagingSenderId: "425007320957"
  }



const appRoutes:Routes = [
  {path:'invoices', component:InvoicesComponent},
  {path:'invoice-form', component:InvoiceFormComponent},
  {path:'', component:InvoiceFormComponent},
  {path:'', component:InvoicesComponent},
  {path:'**', component:PageNotFoundComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    ProductComponent,
    ProductsComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }