import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {
  //private _url = 'http://jsonplaceholder.typicode.com/users';

  productsObservable;
  getproducts(){
    //return this._http.get(this._url).map(res =>res.json()).delay(2000)
    this.productsObservable = this.af.database.list('/products');
    return this.productsObservable;
 
  }

  addProduct(product){
    this.productsObservable.push(product);
  }

  updateProduct(product) {
    let productKey = product.$key;
    let productData = {name:product.pname, cost:product.cost, categoryId:product.categoryId};
    this.af.database.object('/products/' + productKey).update(productData);
  }

  deleteProduct(product){
    let productKey = product.$key;
    this.af.database.object('/products/' + productKey).remove();
  }
  
  constructor(private af:AngularFire) { }

}
